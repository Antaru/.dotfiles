#.dotfiles
Super nice bash stuff.

##Installation
Run this in your home folder (~)
```bash
$ git clone git@bitbucket.org:[yourname]/.dotfiles.git
```
Paste this at the top of your .bash_profile.
```bash
for file in ~/.dotfiles/.{extra,bash_prompt,exports,aliases,functions}; do
    [ -r "$file" ] && source "$file"
done
unset file

# init z   https://github.com/rupa/z
. ~/code/z.sh
```

Update some ssh information for scpp and lss functions in `.functions`

##Function list
`TODO`